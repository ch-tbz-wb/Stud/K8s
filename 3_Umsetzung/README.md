# Umsetzung

 - Bereich: Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen, Cloud-native
 - Semester: 4

## Lektionen

* Präsenzlektionen (Vor Ort): 60
* Präsenzlektionen (Distance Learning): 20
* Selbststudium: 50

## Voraussetzungen

* Microservices
* NoSQL

## Dispensation 

* Kubernetes Zertifizierung (Certified Kubernetes Administrator (CKA) o.ä.)

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

Container, Kubernetes, Microservices

## Lerninhalte

- Kann die grundlegenden Container Standards und Technologien erklären und anwenden, einschliesslich der Erstellung, Verwaltung und des Betriebs von Containern in einer Cloud-nativen Umgebung.
- Versteht den Aufbau und die Funktionsweise von Container-Umgebungen, einschliesslich der Erstellung und Nutzung von Container-Images und der Verwaltung von Container-Registries.
- Kann die Kernkomponenten und Architektur von Kubernetes erläutern und grundlegende Kubernetes-Ressourcen für die Orchestrierung von Containern effektiv nutzen.
- Ist fähig, einen Kubernetes-Cluster von Grund auf aufzusetzen, zu konfigurieren und zu verwalten, einschliesslich der Integration mit externen Ressourcen und Diensten.
- Versteht die Konzepte und Anwendungen von Pods, Services und Ingress in Kubernetes zur Gewährleistung einer effizienten Verteilung und Zugänglichkeit von Anwendungen.
- Kann Deployment-Strategien in Kubernetes entwerfen und implementieren, einschliesslich der Nutzung von Persistent Volumes, Claims und Storage Classes für die Datenpersistenz sowie Config Maps und Secrets für die Konfigurationsverwaltung.
- Versteht die Sicherheitsaspekte in Cloud-nativen Umgebungen, einschliesslich der Implementierung von Netzwerkrichtlinien, Security Contexts für Pods und Anwendungen, und kann Jobs, Daemon Sets und Stateful Sets zur Unterstützung spezifischer Anwendungsfälle einsetzen.

## Übungen und Praxis

* Kubernetes Übersicht (Systemprozesse, Master, Worker)
* Kubernetes Distributionen (Vergleich microk8s, kind, ???)
* Kubernetes Cluster aufsetzen
* Pods
* Services
* Ingress
* Deployment
* Persisten Volumes, Claim, Storage Classes
* Config Map
* Security
* Jobs
* Daemon Sets
* Statefull Sets

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Kubernetes Cluster Umgebung