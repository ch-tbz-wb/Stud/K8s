###
#   Kubernetes Umgebung 3 x ControlPlan, 3 x Worker
#

module "controlplane-01" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no}-cnc-controlplane-01"
  description = "Kubernetes Master"
  userdata    = "cloud-init-cnc-controlplane.yaml"

  cores   = 4
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}

module "controlplane-02" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no + 1}-cnc-controlplane-02"
  description = "Kubernetes Master"
  userdata    = "cloud-init-cnc-worker.yaml"

  cores   = 2
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}

module "controlplane-03" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no + 2}-cnc-controlplane-03"
  description = "Kubernetes Master"
  userdata    = "cloud-init-cnc-worker.yaml"

  cores   = 2
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}

module "worker-01" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no + 3}-cnc-worker-01"
  description = "Kubernetes Worker"
  userdata    = "cloud-init-cnc-worker.yaml"

  cores   = 2
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}

module "worker-02" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no + 4}-cnc-worker-02"
  description = "Kubernetes Worker"
  userdata    = "cloud-init-cnc-worker.yaml"

  cores   = 2
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}


module "worker-03" {
  #source     = "./terraform-lerncloud-module"
  #source = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module      = "${terraform.workspace}-${var.host_no + 5}-cnc-worker-03"
  description = "Kubernetes Worker"
  userdata    = "cloud-init-cnc-worker.yaml"

  cores   = 2
  memory  = 4
  storage = 16
  ports   = [22, 80, 16443, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
}
