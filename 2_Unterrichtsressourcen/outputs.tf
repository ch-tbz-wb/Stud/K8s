###
#   Outputs wie IP-Adresse und DNS Name
#  

output "ip_vm" {
  value       = module.controlplane-01.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm" {
  value       = module.controlplane-01.fqdn_vm
  description = "The FQDN of the server instance."
}

output "description" {
  value       = module.controlplane-01.description
  description = "Description VM"
}

# Einfuehrungsseite(n)

locals {
  ip_01   = module.worker-01.ip_vm
  fqdn_01 = module.worker-01.fqdn_vm
}

output "README" {
  value = templatefile("INTRO.md", {
    ip      = module.controlplane-01.ip_vm,
    fqdn    = module.controlplane-01.fqdn_vm,
    ip_01   = local.ip_01,
    fqdn_01 = local.fqdn_01,
  })
}

