Kubernetes
==========

Umgebung zum Modul: [CNC - Cloud-native Core](https://gitlab.com/ch-tbz-wb/Stud/cnc).

Mit Control-Plane verbinden und `microk8s add-node` ausführen. Die Ausgabe ist dann jeweils, mittels Voranstellung von `sudo`, auf dem Worker auszuführen.

    ssh ubuntu@${ip}
    
    microk8s add-node --token-ttl 3600
    exit
    
    ssh ubuntu@${ip_01}
    sudo <Ausgabe von oben>
    exit
    
Die obigen Befehle sind für jeden Worker zu wiederholen.  
    
Dashboard
---------

Das Kubernetes Dashboard ist wie folgt erreichbar.

    https://${fqdn}:8443

