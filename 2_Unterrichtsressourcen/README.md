# Unterrichtsressourcen 

[[_TOC_]]

*Überblick über die Unterrichtsressourcen*

*Ressourcen in Unterordnern ablegen und in dieser Übersichtsseite sinnvolle Hinweise und Verlinkungen ergänzen*

* [Unterrichtsressource A](A/)
* [Unterrichtsressource B](B/)


## Infrastruktur erstellen
    
    git clone https://gitlab.com/ch-tbz-wb/Stud/cnc
    cd cnc/2_Unterrichtsressourcen
    
    terraform init
    
    terraform plan
    
    terraform apply -parallelism=1 -auto-approve 
    
**Hinweis**: Terraform verwendet 10 Threads das ist zuviel für das MAAS.    
    
Für mehrere Studenten

    export TF_VAR_host_no=10
    for stud in 01 02 03 04 05 06 07 08 09 10 11 12
    do
        terraform workspace new ws${stud}
        terraform workspace select ws${stud}
        terraform init
        terraform apply -parallelism=1 -auto-approve
        TF_VAR_host_no=$((TF_VAR_host_no + 6))
    done
        
**Hinweis**: falls MAAS sich mit Fehler meldet, einloggen in den entsprechende KVM-Host und VMs mittels `virsh list --all` und `virsh undefine xxx` manuell weglöschen.

Der Zugriff, ohne Password, kann mittels [lerncloud](https://github.com/mc-b/lerncloud/raw/main/ssh/lerncloud) SSH-Key erfolgen.

    ssh -i ~/.ssh/lerncloud ubuntu@ws01-10-cnc-controlplane-01.maas
    
### Cluster joinen

    export TF_VAR_host_no=10
    for stud in 01 02 03 04 05 06 07 08 09 10 11 12
    do 
        CPLANE="ws${stud}-${TF_VAR_host_no}-cnc-controlplane-01.maas"
        JOIN="$(ssh -i ~/.ssh/lerncloud ubuntu@${CPLANE} -- microk8s add-node --token-ttl 3600 | sed -n '2p')"
        for cp in 02 03
        do
            TF_VAR_host_no=$((TF_VAR_host_no + 1))
            ssh -i ~/.ssh/lerncloud ubuntu@ws${stud}-${TF_VAR_host_no}-cnc-controlplane-${cp}.maas -- ${JOIN}
        done
        for worker in 01 02 03
        do
            TF_VAR_host_no=$((TF_VAR_host_no + 1))
            ssh -i ~/.ssh/lerncloud ubuntu@ws${stud}-${TF_VAR_host_no}-cnc-worker-${worker}.maas -- ${JOIN} --worker
        done
        TF_VAR_host_no=$((TF_VAR_host_no + 1))
    done  
    
Joint einen Kubernetes Cluster mit 3 x Control Plan und 3 x Worker.    

Kontrollieren

    ssh -i ~/.ssh/lerncloud ubuntu@ws01-10-cnc-controlplane-01.maas
    
    kubectl get nodes -o wide
    kubectl get nodes -l node.kubernetes.io/microk8s-controlplane=microk8s-controlplane
    kubectl get nodes -l node.kubernetes.io/microk8s-worker=microk8s-worker    
    
### Einzelne Node (module) löschen

Wurde die Node bereits dem Kubernetes Cluster hinzufügen, ist diese zuerst zu entfernen `microk8s leave`. Mehr Informationen [hier](https://microk8s.io/docs/clustering).

Dann kann die Node einzeln gelöscht werden:

    terraform destroy -target=module.controlplane-01 -auto-approve 
    
Erstellen einfach wieder mit:

    terraform apply -parallelism=1 -auto-approve  
    
oder gezielt mit:

    terraform apply -target=module.controlplane-01
    
### Container Image erstellen (nicht für Produktive Umgebungen)

Auf der `controlplan-01` ist neben `docker` und `podman` auch `podman-compose` installiert.

Mit allen drei können Container Images erstellt werden. Diese sind dann im jeweiligen Image Verzeichnis des Tools abgelegt und für `microk8s` nicht direkt erreichbar.

**Lösung**: Container Images ins `tar` Format exportieren und in `microk8s` importieren.

    docker save catalog -o catalog.tar
    microk8s ctr catalog import catalog.tar
    
Beim Starten mit Kubernetes die `IfNotPresent` Policy verwenden.

    kubectl run catalog --image catalog --image-pull-policy="IfNotPresent" --restart=Never
    
Wurde schon ein Cluster erstellt, d.h. es sind mehrere Kubernetes Nodes vorhanden, ist der Container explizit auf der ersten Node zu starten:

    kubectl run catalog --image catalog --image-pull-policy="IfNotPresent" --restart=Never \
    --overrides='{ "apiVersion": "v1", "spec": { "nodeName": "ws01-10-cnc-controlplane-01" } }'     
               
      
### Aufräumen
       
    export TF_VAR_host_no=10
    for stud in 01 02 03 04
    do
        terraform workspace select ws${stud}    
        terraform destroy -auto-approve
        TF_VAR_host_no=$((TF_VAR_host_no + 6))
    done  
    
## Weitere Microservices

Wo finde ich weitere Applikationen und Microservices zum Üben

* [Monolithische Applikation](https://github.com/mc-b/duk/blob/master/data/jupyter/autoshop/IntroAutoShop.ipynb)
* [Einfacher WebShop implementiert als Microservice-Applikation](https://github.com/mc-b/duk/blob/master/data/jupyter/autoshop-ms/IntroAutoShop-ms.ipynb)              